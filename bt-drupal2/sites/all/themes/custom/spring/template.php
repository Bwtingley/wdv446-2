<?php
	
	function spring_preprocess_page(&$variables){
		$page = $variables['page'];
		$sidebar_right_class = 'one-fourth sidebar-wrapper';
		$sidebar_left_class = 'one-fourth  sidebar-wrapper';
		$content_class = 'one-half';
		$variables['page']['tpl_control'] = [];

		if(empty($page['sidebar_left'])){
			$sidebar_left_class = '';
			$content_class = 'three-fourths';
				if(empty($page['sidebar_right'])){
					$sidebar_right_class = '';
					$content_class = 'full-width';
				}		
		}

		if(empty($page['sidebar_right'])){
			$sidebar_right_class = '';
			$content_class = 'three-fourths';
				if(empty($page['sidebar_left'])){
					$sidebar_left_class = '';
					$content_class = 'full-width';
				}
		}

		$variables['page']['tpl_control']['sidebar_right_class'] = $sidebar_right_class;
		$variables['page']['tpl_control']['sidebar_left_class'] = $sidebar_left_class;
		$variables['page']['tpl_control']['content_class'] = $content_class;


	}